% I was completely unable to do this otherwise

% read in material properties.inp

addpath('SupportingTwinFiles')

FileID = fopen('preprocessing/propertiesFeature.out','r');
formatSpec = '%f %f %f %i %f \n'; 

sizeA = [5 Inf];
A = fscanf(FileID,formatSpec,sizeA);
fclose(FileID)


A = A';
EulerAngs = A(:,1:3);

nAngles = length(A);

%
misorientationTol = 1.5; % 2 degrees 


nMatches = nAngles * (nAngles-1)/2;
Misorientations = zeros(nMatches,1);

MisorientationMap = zeros(nAngles,nAngles);

TwinPotentialMap = zeros(nMatches,2);


counter = 0;
for ii = 2:nAngles
    for jj = ii+1:nAngles
        
        iAngle = EulerAngs(ii,:)';
        jAngle = EulerAngs(jj,:)';

        R_1 = RMatOfBunge(iAngle, 'radians' );
        R_2 = RMatOfBunge(jAngle, 'radians' );
    
        dR1= inv(R_1)*R_2;
        dq1 = ToFundamentalRegionQ( QuatOfRMat( dR1 ), CubSymmetries());
        [orientation_axis, ang1] = AxisAngleOfQuat( dq1 );
        counter = counter +1;
        Misorientations(counter) = ang1;
        MisorientationMap(ii,jj) = ang1;
        
    end   
end      
    
%figure
%hist(Misorientations,63);
%disp(Misorientations)

Twincounter = 0;
for ii = 2:nAngles
    for jj = ii+1:nAngles
        Diff2Twin = abs(MisorientationMap(ii,jj)-60);
        if Diff2Twin < misorientationTol
           Twincounter = Twincounter +1;
           TwinPotentialMap(Twincounter,1) = ii;
           TwinPotentialMap(Twincounter,2) = jj;
        end
               
    end
end

TwinPotentialMap = TwinPotentialMap(1:Twincounter,:);

% lets export this. We can do whatever afterwards 

disp(Twincounter/counter)



formatSpec = '%i %i \n';
FileID = fopen('TwinPotentialMap.inp','w');
fprintf(FileID,formatSpec,TwinPotentialMap')
fclose(FileID)





        