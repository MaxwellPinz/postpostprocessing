function [] = ReadPltIntoData
FileName = 'plotsUnDeformedConfiguration_displacement.plt';
OutputIPFileName = '../../../../CrackOutputData.mat';
OutputNodalFileName = '../../../../CrackNodalOutputData.mat';
%CrackLocationFileName = 'Crack1.txt';

FileID = fopen(FileName,'r');
FirstLine = fgetl(FileID);
%%% Lets put this all into an excell file
startAndEndValues = strfind(FirstLine,'"');
ColNames = {};
columnCounter = 0;
for ii = 1:2:length(startAndEndValues)-1
    columnCounter = columnCounter +1;
    lowInd = startAndEndValues(ii)+1;
    hiInd = startAndEndValues(ii+1)-1;
    writeStr = FirstLine(lowInd:hiInd);
    ColNames{columnCounter} = writeStr; %#ok<SAGROW>
    % I may expand this later to write an excell sheet
end
nColumns = columnCounter-3;

%%%
SecondLine = fgetl(FileID);
%SeperatedSecondLine = split(SecondLine,',');

expression = ',';
SeperatedSecondLine = regexp(SecondLine,expression,'split');

for ii = 2:3 % oh okay your looping over these things
    eval(SeperatedSecondLine{ii}) % God DAMNIT MAX this is agianst every coding guideline we have ahh
end % oh that was fast

Numbers = fscanf(FileID,'%f');
fclose(FileID);
nodalLocationData = zeros(nodes,3);
LinearCounter = 1;
for iCol = 1:3
    nodalLocationData(:,iCol) = Numbers(LinearCounter:LinearCounter + nodes -1);
    LinearCounter = LinearCounter + nodes;
end
IntegrationPointData = zeros(elements,nColumns);

for iCol = 1:nColumns
    IntegrationPointData(:,iCol) = Numbers(LinearCounter:LinearCounter + elements -1);
    LinearCounter = LinearCounter + elements;
end

RemainderOfNums = Numbers(LinearCounter:end);
Connectivity = reshape(RemainderOfNums,[4,elements]);
Connectivity = Connectivity';
IPLocations = zeros(elements,3);

for iDim = 1:3
    tempIPLoc = zeros(elements,1);
    for iNode = 1:4
        tempIPLoc = tempIPLoc + nodalLocationData(Connectivity(:,iNode),iDim);
    end
    IPLocations(:,iDim) =  tempIPLoc./4;
end
% lets read in the coordinates

save(OutputIPFileName,'IPLocations','IntegrationPointData','ColNames')
% this is the easy way.
% I kind of watn to write an IPLocations.txt, IntegrationPointData.txt, and
% a ColNames list

FileID = fopen('../../../../IPLocations.txt','w');
FormatStr = [repmat('%e ',1,iDim),'\n'];
fprintf(FileID,FormatStr,IPLocations');
fclose(FileID);


FileID = fopen('../../../../IntegrationPointData.txt','w');
FormatStr = [repmat('%e ',1,nColumns),'\n'];
fprintf(FileID,FormatStr,IntegrationPointData');
fclose(FileID);

FileID = fopen('../../../../ColNames.txt','w');
for ii = 1:length(ColNames)
    FormatStr = ['%s \n'];
    fprintf(FileID,FormatStr,ColNames{ii});
end
fclose(FileID);

end
   