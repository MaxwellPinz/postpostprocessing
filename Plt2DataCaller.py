import os 
import shutil
import numpy as np 
import subprocess
import re
import sys
import glob
import traceback
from datetime import datetime
from time import mktime

cwd = os.getcwd()

CurrentDirectoryList = os.listdir()
PLTFileName = 'plotsUnDeformedConfiguration_displacement.plt'

def FindNewestFile(DirectoryList):
   folderCounter = 0
   listLength = len(DirectoryList)
   TimesOfCreation = np.zeros([listLength,1])
   for folder in DirectoryList:
      dateStr = folder.rsplit("analysis_",1)
      # print(dateStr)
      # print(folder)
      TimeOfCreationDateTime = datetime.strptime(dateStr[1],"%Y-%m-%d-%H-%M-%S-%f")
      TimesOfCreation[folderCounter] = mktime(TimeOfCreationDateTime.timetuple())
      folderCounter = folderCounter +1

   idxNewestFolder = np.argmax(TimesOfCreation)

   return idxNewestFolder


def FindNewestPostprocessFile(DirectoryList):
   folderCounter = 0
   listLength = len(DirectoryList)
   TimesOfCreation = np.zeros([listLength,1])
   for folder in DirectoryList:
      dateStr = folder.rsplit("process_",1)
      #print(dateStr)
      # print(folder)
      TimeOfCreationDateTime = datetime.strptime(dateStr[1],"%Y-%m-%d-%H-%M-%S-%f")
      TimesOfCreation[folderCounter] = mktime(TimeOfCreationDateTime.timetuple())
      folderCounter = folderCounter +1

   idxNewestFolder = np.argmax(TimesOfCreation)

   return idxNewestFolder




def FindIndexesOfSubstringsInStr(SubStr,Str):
   A = [m.start() for m in re.finditer(SubStr,Str)]
   return(A)



DataFolder = 'DataStorage'
# check for folders existance 
if os.path.exists(DataFolder):
   shutil.rmtree(DataFolder)
os.mkdir(DataFolder)

# were going to make the folders for each of the data files and then were going to put the other python program in those files 
# after this were going to submit this so we can parallelzie it as opposed to having to do everything serially 



for ii in range(22):
   ExportFiles = 0
   CrackFolder = "crack"+str(ii+1)
   print(CrackFolder)
   if CrackFolder in CurrentDirectoryList:
      TempPath = cwd + "/" + CrackFolder + "/" + "loading"
      tempDirectoryList = glob.glob(TempPath + "/" + "analysis*" )
      # now we have to convert the string afterwards to date time, then basically take the newest one
      # analysis_2019-07-16-11-07-58-847

      if len(tempDirectoryList) == 0:
         print("crack"+str(ii+1)+ "Has no analysis folders" )
      else:
         newestFolderIdx = FindNewestFile(tempDirectoryList)
         FolderName = tempDirectoryList[newestFolderIdx]
         # generate newest path 

         print(FolderName)
         print(" ")

         PostProcessingPath = FolderName + "/" + "postprocessing"
         # now we need to determine which is the newest postprocessing file
         PostProcessingDirectoryList = glob.glob(PostProcessingPath + "/" + "process*" )
         # print(PostProcessingDirectoryList)
         # print(PostProcessingPath)
         if len(PostProcessingDirectoryList) == 0:
            print("crack"+str(ii+1)+ "Has no postprocessing folders" )
         else:
            newestFolderIdx = FindNewestPostprocessFile(PostProcessingDirectoryList)
            FolderNamePostProcess = PostProcessingDirectoryList[newestFolderIdx] 

            PLTFilePath = FolderNamePostProcess + "/" + PLTFileName  # os path join 

            iSimFolder = DataFolder + '/' + 'crack' + str(ii+1)
            os.mkdir(iSimFolder)

            # okay lets copy the python into the new folders. The only reason for this is i dont want to generate a shit ton of output files in the main folder  files into the new paths 

            # copy python line 
            # write the qsub code 
            SubmissionText = ['#!/bin/bash -l',
            '',
            '#SBATCH -A sghosh20-condo',
            '#SBATCH --job-name=PostProcessing',
            '#SBATCH --time=05:59:59',
            '#SBATCH --nodes=1',
            '#SBATCH --ntasks-per-node=1',
            '#SBATCH --partition=lrgmem',
            ''
            ]


            SubmissionFile = open(iSimFolder + '/' + 'RunGrainBoundaryFinder.scr','w')
            for item in SubmissionText:
               SubmissionFile.write(item + '\n')
            SubmissionFile.write('python Plt2Data.py ' + PLTFilePath + ' ')     
            SubmissionFile.close()

            # also we should like copy the plt2Data.py 
            shutil.copyfile('Plt2Data.py',os.path.join(iSimFolder,'Plt2Data.py'))
            # now we need to cd into the file, submit the job, and go back home 
            subprocess.run('cd '+ iSimFolder + '; qsub RunGrainBoundaryFinder.scr ;' + 'cd  ' + cwd, shell = True, check = True)

