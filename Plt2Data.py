import os 
import shutil
import numpy as np 
import subprocess
import re
import sys
import glob
import traceback
from datetime import datetime
from time import mktime
import sys

cwd = os.getcwd()
nEntries = len(sys.argv)-1

PLT_FileName = sys.argv[1]
if nEntries >= 2:
   ExportFolderPath = sys.argv[2]
else:
   ExportFolderPath = cwd


def FindIndexesOfSubstringsInStr(SubStr,Str):
   A = [m.start() for m in re.finditer(SubStr,Str)]
   return(A)


def main(PLT_FileName,ExportFolderPath):
   PLT_File = open(PLT_FileName,'r')

   line = PLT_File.readline()
   ColumnNames = []

   ColumnCounter = 0
   SubStr = '"'
   IDX_Quotes = FindIndexesOfSubstringsInStr(SubStr,line)
   nColumnCounts = int(len(IDX_Quotes)/2)
   for iCol in range(nColumnCounts):
    ColString = line[ (IDX_Quotes[iCol*2]+1):IDX_Quotes[iCol*2 +1] ]
    ColumnNames.append(ColString)
   nColumns = nColumnCounts -3 # this is becasue we dont want to count the X Y Z at the begining       


   # read the second line 
   line = PLT_File.readline()
   # from this line we need 2 things 
   # nElements and nNodes
   NodesStr = 'nodes ='
   ElementsStr = 'elements ='
   # lets split this string by ,  loop over it and check fo both elements and nodes  
   IdxNodesStr = line.find(NodesStr)
   IdxElementsStr = line.find(ElementsStr)

   endNodesStr = line.find(',',IdxNodesStr)
   endElementsStr = line.find(',',IdxElementsStr)

   nNodes = int(line[IdxNodesStr + len(NodesStr):endNodesStr])
   nElements = int(line[IdxElementsStr + len(ElementsStr):endElementsStr])

   LinearStrLines = PLT_File.readlines()
   PLT_File.close()
   TotalList = []
   for line in LinearStrLines:
      LineList = [float(x.strip()) for x in line.split()]
      TotalList.extend(LineList)
   
   NpLinear = np.asarray(TotalList)

   # I know the number of columns 
   # I also know how many entries there should be 

   nEntries4Data = nColumns*nElements
   nComponentsConnectivity = 4*nElements
   nNodalEntries = 3*nNodes

   NpNodalCoord = NpLinear[0:nNodalEntries]
   NpData = NpLinear[nNodalEntries:nEntries4Data+nNodalEntries]
   NpConnectivity = NpLinear[(nNodalEntries + nEntries4Data):len(NpLinear)]

   if (len(NpConnectivity) == nComponentsConnectivity):
      print('The number of components is correct!')

   # reshape the arrays 
   NpNodalCoord = np.reshape(NpNodalCoord,[nNodes,3],order='F') # this probably wont work at all 
   NpData = np.reshape(NpData,[nElements,nColumns],order='F')
   NpConnectivity = np.reshape(NpConnectivity,[nElements,4],order='C')

   # I should compute a few things now like the volume 

   Ip_Positions = np.zeros([nElements,3])
   Ip_Volumes = np.zeros([nElements,1])

   # calculate the volumes 
   for iElement in range(nElements):
      Coords = np.zeros([4,3])
      for iDim in range(3):
         elementNumbers = NpConnectivity[iElement,:]
         #print(elementNumbers)
         Coords[:,iDim] = [ NpNodalCoord[int(ii)-1,iDim] for ii in elementNumbers ]
      a = Coords[0,:]
      b = Coords[1,:]
      c = Coords[2,:]
      d = Coords[3,:]

      v1 = a-d
      v2 = b-d
      v3 = c-d
      
      Volume =  np.dot(np.cross(v2,v3),v1) / 6
      Ip_Volumes[iElement]=Volume
      # now that i have the coords, lets calculate the volume
   for iElement in range(nElements):
      for iDim in range(3):
         elementNumbers = NpConnectivity[iElement,:]
         #print(elementNumbers)
         NodalCoords = [ NpNodalCoord[int(ii)-1,iDim] for ii in elementNumbers ]
         Ip_Positions[iElement,iDim] = np.mean(NodalCoords)

   # now everything has been calculated. We should either return the things or write them 
   FileFormat = '%e '   
   np.savetxt(ExportFolderPath + '/' + 'CrackData.inp',NpData,FileFormat,delimiter = ',')
   np.savetxt(ExportFolderPath + '/' + 'Ip_Volume.inp',Ip_Volumes,FileFormat)
   np.savetxt(ExportFolderPath + '/' + 'Connectivity.inp',NpConnectivity,FileFormat,delimiter = ',')
   np.savetxt(ExportFolderPath + '/' + 'IP_Positions.inp',Ip_Positions,FileFormat,delimiter = ',')
   np.savetxt(ExportFolderPath + '/' + 'NodalPositions.inp',NpNodalCoord,FileFormat,delimiter = ',')

   ColumnNamesFile = open(ExportFolderPath + '/' + 'ColumnNames.inp','w')
   for item in ColumnNames:
      ColumnNamesFile.write(item + '\n')
   ColumnNamesFile.close()
main(PLT_FileName,ExportFolderPath)