# the goal of this code is to read in both the possible twin lists and the outputs of the simulation. 
# then we wish to determine which experimental points are within some distace from a grain boundary 

# read in the data corresponding to the feature IDs as well as the coordinates 

import numpy as np 
import numpy.matlib


# read in probably twin-feature ID list 

CoordFile = 'IPLocations.txt'
DataFile = 'IntegrationPointData.txt'
TwinPotentialFile = 'TwinPotentialMap.inp'

FileNameIDX_TwinnedElements = 'TwinnedElementIDX.txt'
TwinID_FileName = 'TwinID.txt'

ElementCentroids = np.loadtxt(CoordFile)

IP_Data = np.loadtxt(DataFile)


ProbableTwins = np.loadtxt(TwinPotentialFile)
nProbableTwins = len(ProbableTwins)


nElements = len(IP_Data)

# important user defined parameters
MinDistBetweenPts = 3e-6 # microns Maybe i have to include the 10^-6?

print(MinDistBetweenPts)

#ElementCentroids = np.zeros 
ElementIDvalues = IP_Data[:,0]

# for ii in range(183):
#   temp1 = np.ones(nElements)
#   print(np.sum(temp1[ElementIDvalues==ii]))



GlobalIndeces = np.arange(nElements)

IDX_TwinElements = np.array([])

IDX_TwinList = []

for iTwinPair in range(nProbableTwins):
  twinPair = ProbableTwins[iTwinPair,:]
  iGrainID = twinPair[0]
  jGrainID = twinPair[1]
  
  XYZ_GrainI = ElementCentroids[ElementIDvalues==iGrainID,:]
  XYZ_GrainJ = ElementCentroids[ElementIDvalues==jGrainID,:]
  
  # print(iGrainID)
  # print(jGrainID)

  Global_IDX_GrainI = GlobalIndeces[ElementIDvalues==iGrainID]
  Global_IDX_GrainJ = GlobalIndeces[ElementIDvalues==jGrainID]
  
#   LocalIndeces_I = np.arange(len(Global_IDX_GrainI))
#   LocalIndeces_J = np.arange(len(Global_IDX_GrainJ))
  
  
  # unfortunately this is an n^2 operation so .....yay? 
  # lets cut it by 2, but its still n^2. grrrrrrrrrr
  
  # vectorization 
  jCounter = 0
  for j_ElementInGrain in XYZ_GrainJ:
    Diff = XYZ_GrainI - np.matlib.repmat(j_ElementInGrain,len(XYZ_GrainI),1) # this will fail if there are fewer than 3 elements in Grain J 
    
    # print(Diff)
    # print(XYZ_GrainI)
    # pause

    Dist = np.sqrt(np.sum(np.square(Diff),1)) # this is now the distances. Now i need to check if any of the distances meet the criteria 
    
    if any(Dist[Dist <= MinDistBetweenPts]):
      IDX_InRange = Global_IDX_GrainI[Dist <= MinDistBetweenPts]
      #print(IDX_InRange)
      IDX_TwinElements = np.append(IDX_TwinElements,IDX_InRange)
      IDX_TwinElements = np.append(IDX_TwinElements,Global_IDX_GrainJ[jCounter])
      # IDX_TwinElements.append(IDX_InRange)
      #IDX_TwinElements.append(Global_IDX_GrainJ[jCounter])
      IDX_TwinList.append(iTwinPair)
      
    jCounter = jCounter+1  


    
print(IDX_TwinElements)   
IDX_ElementsAtTwinBoundary = np.unique(IDX_TwinElements)

Format = '%i'
print('There are '+str(len(IDX_ElementsAtTwinBoundary))+' Elements in the Twin Reigon')
np.savetxt(FileNameIDX_TwinnedElements,IDX_ElementsAtTwinBoundary,Format)

print(np.unique(IDX_TwinList))
print(len(np.unique(IDX_TwinList))/nProbableTwins)    

UnqueIDX_Twins = np.unique(IDX_TwinList)
print('There are '+str(len(UnqueIDX_Twins))+' Twinned Grains')
FeaturePropertiesTwinList = ProbableTwins[UnqueIDX_Twins,:]

Format = '%i %i'
np.savetxt(TwinID_FileName,FeaturePropertiesTwinList,Format)
    
    
    
    
    