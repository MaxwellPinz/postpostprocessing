import os 
import shutil

LocalDir = os.listdir()

for item in LocalDir:
   if os.path.isdir(item):
      try:
         shutil.rmtree('../'+item)
      except:
         pass   
      shutil.copytree(item,'../'+item)
   if os.path.isfile(item):
      try:
         shutil.rmtree('../'+item)
      except:
         pass 
      shutil.copyfile(item,'../'+item)